package com.example.firstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener{

    private Button buttonWithoutLogic, buttonToShowToast;
    private EditText editTextInputString;
    private TextView textViewShowInputString, textViewShowSwitchProgress, textViewShowsProgressSeekBar;
    private Switch switch1;
    private CheckBox checkBox1, checkBox2, checkBox3;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonWithoutLogic = findViewById(R.id.buttonWithoutLogic);
        buttonToShowToast = findViewById(R.id.buttonToShowToast);
        editTextInputString = findViewById(R.id.editTextInputString);
        textViewShowInputString = findViewById(R.id.textView);
        textViewShowSwitchProgress = findViewById(R.id.textView2);
        textViewShowsProgressSeekBar = findViewById(R.id.textViewShowsProgressSeekBar);
        switch1 = findViewById(R.id.switch1);
        checkBox1 = findViewById(R.id.checkBox);
        checkBox2 = findViewById(R.id.checkBox2);
        checkBox3 = findViewById(R.id.checkBox3);
        seekBar = findViewById(R.id.seekBar);

        buttonWithoutLogic.setOnClickListener(this);
        buttonToShowToast.setOnClickListener(this);
        checkBox1.setOnClickListener(this);
        checkBox2.setOnClickListener(this);
        checkBox3.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchLogic(isChecked);
            }
        });

    }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            textViewShowsProgressSeekBar.setText(String.valueOf(seekBar.getProgress()));
        }



    public void setTextOnTextView(String msg, TextView textView){
        textView.setText(msg);
    }

    public void toastView(String msg){
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonWithoutLogic:
                toastView("Button without logic is clicked");
                break;
            case R.id.buttonToShowToast:
                String resultString = editTextInputString.getText().toString();
                toastView(resultString);
                setTextOnTextView(resultString,textViewShowInputString);
                break;
            case R.id.checkBox:
                toastView("CheckBox1 Activated");
                break;

            case R.id.checkBox2:
                toastView("CheckBox2 Activated");
                break;
            case R.id.checkBox3:
                toastView("CheckBox3 Activated");
                break;
        }
    }

    void switchLogic(boolean isChecked){
        if (isChecked){
            setTextOnTextView("ON",textViewShowSwitchProgress);
        }
        else{
            setTextOnTextView("OFF",textViewShowSwitchProgress);
        }
    }

}